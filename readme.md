# Dark Brackets for Aptana Studio 3.x

Dark background, colorful and content-focused color theme for Aptana Studio 3.x.

![Dark Brackets Theme][1]

[1]: https://bitbucket.org/druellan/dark-brackets-theme-for-aptana-studio/downloads/DarkBracketsScreen.png